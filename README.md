# Ultima-online-outlands-linux

installation guide for ultima online on linux (Ubuntu 20.04)

## Step 1 - First install wine-staging and winetricks:
```bash
wget -nc https://dl.winehq.org/wine-builds/winehq.key

sudo apt-key add winehq.key

echo "deb https://dl.winehq.org/wine-builds/ubuntu/ $(lsb_release -c --short) main" | sudo tee /etc/apt/sources.list.d/wine.list

sudo apt update && sudo apt install wine-staging winetricks
```
## Step 2 - Download the next 2 files for to avoid checksum error from archive.org
Download 2 files:
- [VC x86](https://web.archive.org/web/20200415100528/https://aka.ms/vs/16/release/VC_redist.x86.exe)
- [VC x64](https://web.archive.org/web/20200414214617/https://aka.ms/vs/16/release/VC_redist.x64.exe)

Copy VC_redist.x86.exe and VC_redist.x64.exe to _~/.cache/winetricks/vcrun2019_ and rename them to lowercase then changing VC_redist.x86.exe -> vc_redist.x86.exe and  VC_redist.x64.exe -> vc_redist.x64.exe

## Step 3 - Open winetricks in a terminal
winetricks #In your winetricks window create a new wineprefix named 'outlands' (no quotes) and select 64 bits
	
#### Step 3.1 - Now inside your wine prefix in the wintricks window install the following at the frist option 
d3dx9 faudio dotnet48 vcrun2019 d3dcompiler_47

# Step 4 - Download from your browser the .NET Core Desktop 
[Runtime](https://dotnet.microsoft.com/download/dotnet/thank-you/runtime-desktop-5.0.3-windows-x64-installer)
And run the following command in terminal at Downloads folder:
```bash
WINEPREFIX=~/.local/share/wineprefixes/outlands/ wine windowsdesktop-runtime-5.0.3-win-x64.exe
```

# Step 5 - Finally download Outlands from official page 
And run this command:
```bash
WINEPREFIX=~/.local/share/wineprefixes/outlands/ wine Outlands.exe
```

*If using amd gpu then you can install dxvk. If using intel or nvidia then you might have to run winetricks rendered=gdi if the game says it detects no graphics card.

Happy play :slight_smile:

Special Thanks to: @wannabe
